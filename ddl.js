
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path')
  , config = require('./config')
  , Push = require( 'pushover-notifications' );

var app = express();
var port = +process.env.PORT || 1234;

// all environments
app.set('port', port);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.bodyParser({ keepExtensions: true, uploadDir: '/data/ddl/uploads' }));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// app.get('/img', express.static(__dirname + '/img'));
// app.use(express.static(path.join(__dirname, '/img')));
app.get('/test123', function (req, res)  {
  var auth = req.headers.authorization;
  var user;

  auth = auth.split(' ')[1];
  auth = (new Buffer(auth, 'base64')).toString();
  user = auth.split(':')[0];

  res.send(user);
});
app.get('/listdir/:max', routes.listdir);
app.get(/\//, routes.index);
app.post('/mkdir', routes.mkdir);
app.post('/newtorrent', routes.newtorrent);


config.push = new Push({
  token: config.pushover.token,
  user: config.pushover.user
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
