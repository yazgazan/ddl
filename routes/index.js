
var glb = require('glb');
var _path = require('path');
var fs = require('fs');
var exec = require('child_process').exec;
var mkdirp = require('mkdirp');
require('sugar');

var getUserFromReq = function getUserFromReq(req)  {
  var auth = req.headers.authorization;
  var user;

  auth = auth.split(' ')[1];
  auth = (new Buffer(auth, 'base64')).toString();
  user = auth.split(':')[0];

  return user;
};

/*
 * GET home page.
 */

exports.index = function(req, res){
  var fullPath;
  var stat;
  var filesList;
  var files;
  var path;
  
  path = decodeURIComponent(req.path);
  if (path.slice(0, 5) == '/img/')  {
    res.sendfile(glb.rootPath + req.path);
    return;
  }
  fullPath = _path.join(glb.basePath, path);
  try  {
    stat = fs.statSync(fullPath);
  } catch(e)  {
    res.end('failed');
    console.log(e);
    return;
  }
  if (stat.isFile() === true)  {
    try  {
      res.download(fullPath);
    } catch (e)  {
      console.log(e);
    }
    return;
  }
  if (stat.isDirectory() === false)  {
    res.end('error');
    return;
  }
  filesList = fs.readdirSync(fullPath);
  files = filesList.map(function (fileName)  {
    var filePath = _path.join(fullPath, fileName);
    var file = {};
    var filestat;
    if (fileName.charAt(0) === '.')
      return undefined;
    try  {
      filestat = fs.statSync(filePath);
    } catch (e)  {
      return undefined;
    }
    file.path = _path.join(path, fileName);
    file.name = fileName;
    file.dir = filestat.isDirectory();
    file.time = filestat.mtime.format('{dd}-{MM}-{yyyy} {HH}:{mm}');
    file.mtime = filestat.mtime;
    file.size = filestat.size.bytes(2);
    file.type = '';
    file.icon = '/img/unknown.png';
    if (filestat.isDirectory())
      file.type = 'dir';
    else if (filestat.isFile())  {
      file.type = file.name.split('.').splice(-1)[0].toLowerCase();
    }
    for (var id in glb.filetypes)  {
      var type = glb.filetypes[id];

      if (type.exts.indexOf(file.type) != -1)  {
        file.icon = type.icon;
        break;
      }
    }
    return file;
  });
  if (req.cookies.sort === "true") {
    files = files.sort(function (a, b)  {
      return b.mtime - a.mtime;
    });
  } else {
    files = files.sort(function (a, b)  {
      return (a.name.localeCompare(b.name));
    });
  }
  res.render('index', {
    title: 'ddl.yazou.org',
    path: path,
    fullPath: fullPath,
    files: files,
    sort: req.cookies.sort
  });
};

exports.newtorrent = function (req, res)  {
  if (req.files.torrent.size === 0)  {
    res.redirect("back");
    return;
  }
  var filename = req.files.torrent.name.replace('.torrent', '');
  var trm = "transmission-remote transmission.yazou.org:80 --auth yazou:PKRtv532Q"
  exec(trm + " --download-dir '" + req.param("path") + "'", function (error, stdout, stderr)  {
    exec(trm + " --add '" + req.files.torrent.path + "'", function (error, stdout, stderr)  {
        var user = getUserFromReq(req);
        var msg = {
          message: user + " downloaded '" + filename + "' (" + req.param("path") + ")",
          title: "new torrent added (" + user + ")",
          priority: -1
        };
        glb.push.send(msg, function (err, result)  {
          if (err) console.log(err);
          console.log(result);
          if (req.param("origin") === "t411")  {
            res.send("ok");
            return;
          }
          res.redirect("back");
        });
    });
  });
};

exports.mkdir = function (req, res)  {
  var path = req.param("path");
  var name = req.param("name");

  if (!name || name.length === 0)  {
    res.redirect("back");
    return;
  }
  var fullPath = _path.join(path, name);
  mkdirp(fullPath, '0777', function (e)  {
    if (e)  {
      res.redirect("back");
      return;
    }
    fs.chmod(fullPath, 0775, function (e)  {
      var newPath = fullPath.replace(glb.basePath, '');
      res.redirect(newPath);
    });
  });
};

var readDirRecur = function readDirRecur(path, max)  {
  if (max === 0)  {
    return {};
  }
  try  {
    var files = fs.readdirSync(path);
  } catch (e)  {
    return null;
  }
  var ret = new Object;

  for (var id in files)  {

    var filename = files[id];
    var filepath = _path.join(path, filename);

    try  {
      var stats = fs.statSync(filepath);
    } catch (e)  {
      continue;
    }
    if (stats.isDirectory() === false)  {
      continue;
    }
    ret[filename] = readDirRecur(filepath, max - 1);
  }
  return ret;
};

exports.listdir = function (req, res)  {
  var max = req.param("max") || 3;
  res.send(readDirRecur(glb.basePath, max));
};

